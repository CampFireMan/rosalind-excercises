from utils import (dna_to_rna, get_fasta_data, reverse_complement,
                   rna_to_amino, rna_to_amino_list)


def read_orf(amino_list: list, start: int) -> str:
    result = [amino_list[start]]
    for i in range(start + 1, len(amino_list)):
        if amino_list[i] == 'Stop':
            return ''.join(result)
        result.append(amino_list[i])
    return None


workload = []
for string in get_fasta_data('rosalind_orf.txt').values():
    for i in range(0, 3):
        workload.append(rna_to_amino_list(dna_to_rna(string[i:])))
        workload.append(rna_to_amino_list(
            dna_to_rna(reverse_complement(string)[i:])))

result = set()
for string in workload:
    for i, c in enumerate(string):
        if c == 'M':
            orf = read_orf(string, i)
            if orf is not None:
                result.add(orf)

for string in result:
    print(string)
