from utils import get_lines

lines = get_lines('rosalind_hd.txt')
string_a = lines[0]
string_b = lines[1]

distance = 0
for i, c in enumerate(string_a):
    if c != string_b[i]:
        distance += 1
print(distance)
