with open('data/rosalind_motif.txt') as file:
    string = file.readline().rstrip()
    substr = file.readline().rstrip()
    result = []
    for i, c in enumerate(string):
        if string[i:i + len(substr)] == substr:
            result.append(str(i+1))


print(' '.join(result))
