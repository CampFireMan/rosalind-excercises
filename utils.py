CODON_TABLE = {
    'UUU': 'F', 'CUU': 'L',      'AUU': 'I',      'GUU': 'V',
    'UUC': 'F', 'CUC': 'L',      'AUC': 'I',      'GUC': 'V',
    'UUA': 'L', 'CUA': 'L',      'AUA': 'I',      'GUA': 'V',
    'UUG': 'L', 'CUG': 'L',      'AUG': 'M',      'GUG': 'V',
    'UCU': 'S', 'CCU': 'P',      'ACU': 'T',      'GCU': 'A',
    'UCC': 'S', 'CCC': 'P',      'ACC': 'T',      'GCC': 'A',
    'UCA': 'S', 'CCA': 'P',      'ACA': 'T',      'GCA': 'A',
    'UCG': 'S', 'CCG': 'P',      'ACG': 'T',      'GCG': 'A',
    'UAU': 'Y', 'CAU': 'H',      'AAU': 'N',      'GAU': 'D',
    'UAC': 'Y', 'CAC': 'H',      'AAC': 'N',      'GAC': 'D',
    'UAA': 'Stop', 'CAA': 'Q',      'AAA': 'K',      'GAA': 'E',
    'UAG': 'Stop', 'CAG': 'Q',      'AAG': 'K',      'GAG': 'E',
    'UGU': 'C', 'CGU': 'R',      'AGU': 'S',      'GGU': 'G',
    'UGC': 'C', 'CGC': 'R',      'AGC': 'S',      'GGC': 'G',
    'UGA': 'Stop', 'CGA': 'R',      'AGA': 'R',      'GGA': 'G',
    'UGG': 'W', 'CGG': 'R',      'AGG': 'R',      'GGG': 'G',
}


def get_string(filename) -> str:
    with open('data/' + filename, 'r') as file:
        file = file.readlines()
        if len(file) != 1:
            raise ValueError('error')
        return file[0]


def get_lines(filename) -> str:
    with open('data/' + filename, 'r') as file:
        return file.readlines()


def get_fasta_data(filename) -> dict:
    with open('data/' + filename, 'r') as file:
        result = {}
        line = file.readline().rstrip()
        while line:
            label = line[1:]
            string = ''
            line = file.readline().rstrip()
            while not line.startswith('>') and line:
                string += line
                line = file.readline().rstrip()
            result[label] = string
        return result


def dna_to_rna(string):
    return string.replace('T', 'U')


COMPLEMENT = {'A': 'T', 'T': 'A', 'G': 'C', 'C': 'G'}


def reverse_complement(string):
    result = []
    for c in reversed(string):
        result.append(COMPLEMENT[c])
    return ''.join(result)


def rna_to_amino_list(string):
    result = [string[i:i+3] for i in range(0, len(string), 3)]

    for i, codon in enumerate(result):
        if len(codon) == 3:
            amino = CODON_TABLE[codon]
            result[i] = amino
        else:
            result.pop(i)
    return result


def rna_to_amino(string):
    return ''.join(rna_to_amino_list(string))
