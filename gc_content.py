from collections import Counter

from utils import get_fasta_data

data = get_fasta_data('rosalind_gc.txt')
result = ('>', 0.0)

for label, string in data.items():
    n = len(string)
    counter = Counter(string)
    g_content = counter['G']
    c_content = counter['C']
    ratio = ((g_content + c_content) / n) * 100
    if ratio > result[1]:
        result = (label, ratio)

print(result)
