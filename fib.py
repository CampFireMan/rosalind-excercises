from utils import get_lines


def fibonacci(n: int, k: int) -> int:
    prev = 0
    cur = 1
    for i in range(2, n + 1):
        tmp = cur + prev * k
        prev = cur
        cur = tmp
    return cur


line = get_lines('rosalind_fib.txt')[0].split(' ')
n = int(line[0])
k = int(line[1])

print(fibonacci(n, k))
