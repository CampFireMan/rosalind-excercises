from utils import get_fasta_data, reverse_complement


def extract_substrings(string, minimum, maximum) -> list:
    result = []
    for i, c in enumerate(string):
        for length in range(minimum, maximum + 1):
            end = i + length
            if end < len(string) + 1:
                result.append((i, string[i:end]))
    return result


result = []
for string in get_fasta_data('rosalind_restriction_sites.txt').values():
    for pos, substr in extract_substrings(string, 4, 12):
        if substr == reverse_complement(substr):
            result.append((pos + 1, len(substr)))

for tup in result:
    print(str(tup[0]) + ' ' + str(tup[1]))
