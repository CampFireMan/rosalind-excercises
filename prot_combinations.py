from collections import defaultdict

from utils import CODON_TABLE, get_string

combinations = defaultdict(int)
for amino in CODON_TABLE.values():
    combinations[amino] += 1

string = get_string('rosalind_infer.txt')
result = 1
for c in string:
    result *= combinations[c]

result = (result * combinations['Stop']) % (10**6)
print(result)
