from utils import get_lines


def mendel(rr: int, rd: int, dd: int) -> float:
    n = rr + rd + dd
    return 1 - (
        (rr / n) * ((rr-1) / (n-1))
        + (rr / n) * (rd / (n-1)) * 0.5
        + (rd / n) * (rr / (n-1)) * 0.5
        + (rd / n) * ((rd-1) / (n-1)) * 0.25
    )


line = get_lines('rosalind_mendel.txt')[0].split(' ')
dd = int(line[0])
rd = int(line[1])
rr = int(line[2])

print(mendel(rr, rd, dd))
