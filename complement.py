from utils import get_string, reverse_complement

print(reverse_complement(get_string('rosalind_revc.txt')))
