from collections import defaultdict

string = str(input('please enter base string'))
result = defaultdict(int)

for c in string:
    result[c] += 1

print(result)
